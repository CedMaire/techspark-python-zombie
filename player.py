import turtle

class Player:
    player = None

    def turn_left(self):
        self.player.left(10)

    def turn_right(self):
        self.player.right(10)

    def __init__(self, screen, color="white", shape="turtle", position=(0, 0)):
        turtle.delay(0)
        self.player = turtle.Turtle()
        self.player.penup()
        self.player.speed(0)
        self.player.shape(shape)
        self.player.color(color)
        self.player.goto(position)

        screen.onkey(self.turn_left, "a")
        screen.onkey(self.turn_right, "d")
