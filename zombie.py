import turtle
import random

class Zombie:
    screen = None

    zombie = None
    player = None
    bullet = None

    update_timer = 25

    GAME_OVER = False

    score = 0
    score_text = None

    def set_zombie_image(self, x_coord, y_coord):
        direction = None
        if y_coord == -290:
            self.zombie.shape("zombie_up.gif")
        elif y_coord == 290:
            self.zombie.shape("zombie_down.gif")
        elif x_coord == -390:
            self.zombie.shape("zombie_right.gif")
        else:
            self.zombie.shape("zombie_left.gif")

    def die(self):
        if self.zombie.distance(self.bullet.bullet) < 50:
            next_coords = random.choice([(390, random.randint(-290, 290)),
                                        (-390, random.randint(-290, 290)),
                                        (random.randint(-390, 390), 290),
                                        (random.randint(-390, 390), -290)])
            self.set_zombie_image(next_coords[0], next_coords[1])
            self.zombie.goto(next_coords)
            self.bullet.bullet.hideturtle()
            self.update_score()

    def update_score(self):
        self.score += 10
        self.score_text.clear()
        self.score_text.write("Score: " + str(self.score), False, "left", ("Arial", 24, "bold"))

    def kill_player(self):
        if self.zombie.distance(self.player.player) < 50 and not self.GAME_OVER:
            self.GAME_OVER = True
            self.screen.clear()
            game_over_text = turtle.Turtle()
            game_over_text.hideturtle()
            game_over_text.penup()
            game_over_text.speed()
            game_over_text.color("red")
            game_over_text.goto(0, 0)
            game_over_text.write("GAME OVER\n(click to exit)", False, "center", ("Arial", 50, "bold"))
            self.screen.exitonclick()
            return True

    def update(self):
        if self.kill_player():
            return None
        self.die()
        self.zombie.setheading(self.zombie.towards(self.player.player.pos()))
        self.zombie.forward(8)

        self.screen.ontimer(self.update, self.update_timer)

    def __init__(self, screen, player, bullet):
        turtle.delay(0)
        self.screen = screen
        self.player = player
        self.bullet = bullet

        self.zombie = turtle.Turtle()
        self.zombie.penup()
        self.zombie.speed(0)

        next_coords = random.choice([(390, random.randint(-290, 290)),
                                    (-390, random.randint(-290, 290)),
                                    (random.randint(-390, 390), 290),
                                    (random.randint(-390, 390), -290)])
        self.set_zombie_image(next_coords[0], next_coords[1])
        self.zombie.goto(next_coords)

        self.score_text = turtle.Turtle()
        self.score_text.hideturtle()
        self.score_text.penup()
        self.score_text.speed(0)
        self.score_text.color("white")
        self.score_text.goto(-395, 275)
        self.score_text.write("Score: " + str(self.score), False, "left", ("Arial", 24, "bold"))

        self.screen.ontimer(self.update, self.update_timer)
