import turtle

from player import Player
from bullet import Bullet
from zombie import Zombie

turtle.delay(0)

width = 800
length = 600

turtle.setup(width, length)
screen = turtle.Screen()
screen.bgcolor("black")

turtle.addshape("zombie_up.gif")
turtle.addshape("zombie_down.gif")
turtle.addshape("zombie_left.gif")
turtle.addshape("zombie_right.gif")

player = Player(screen)
bullet = Bullet(screen, player)
zombie = Zombie(screen, player, bullet)

screen.listen()

screen.mainloop()
