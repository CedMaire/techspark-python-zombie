import turtle

class Bullet:
    player = None
    bullet = None

    is_firing = False

    def fire(self):
        if not self.is_firing:
            self.is_firing = True

            self.bullet.goto(self.player.player.pos())
            self.bullet.setheading(self.player.player.heading())
            self.bullet.showturtle()

            x, y = self.bullet.pos()
            while abs(x) < 450 and abs(y) < 350:
                self.bullet.forward(15)
                x, y = self.bullet.pos()

            self.is_firing = False

    def __init__(self, screen, player):
        turtle.delay(0)
        self.player = player

        self.bullet = turtle.Turtle()
        self.bullet.hideturtle()
        self.bullet.penup()
        self.bullet.speed(0)
        self.bullet.turtlesize(0.5, 0.5, 0.5)
        self.bullet.shape("square")
        self.bullet.color("white")

        screen.onkey(self.fire, "space")
